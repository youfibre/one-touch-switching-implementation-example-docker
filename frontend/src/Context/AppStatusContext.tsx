import axios from "axios";
import React, { useContext, useEffect, useState } from "react";

type AppStatusContextProps = {
  status: any;
  refreshAppStatus: () => Promise<void>;
};

const AppStatusContext = React.createContext<AppStatusContextProps>({
  status: {},
  refreshAppStatus: async () => {}
});

export function useAppStatus(): AppStatusContextProps {
  return useContext(AppStatusContext);
}

export function AppStatusProvider({ children }: { children: React.ReactNode }): JSX.Element {
  const [loading, setLoading] = useState(true);
  const [status, setStatus] = useState<object>({});

  useEffect(() => {
    if (loading) {
      (async () => {
        await refreshAppStatus();
      })();
    }
  }, [loading]);

  async function refreshAppStatus() {
    const response = await axios.get('http://127.0.0.1:8081/app-status');
    const status = response.data;
    setStatus(status);
    setLoading(false);
  }

  return (
    <AppStatusContext.Provider
      value={{
        status,
        refreshAppStatus
      }}
    >
      {loading ? <></> : children}
    </AppStatusContext.Provider>
  );
}
