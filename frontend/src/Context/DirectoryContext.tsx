import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Directory, RCP } from "../Types";

type DirectoryContextProps = {
  directory: Directory;
  getRcpById: (rcpId: string) => RCP | undefined;
};

const DirectoryContext = React.createContext<DirectoryContextProps>({
  directory: { rcpIdDirectory: {} },
  getRcpById: () => undefined
});

export function useDirectory(): DirectoryContextProps {
  return useContext(DirectoryContext);
}

export function DirectoryProvider({ children }: { children: React.ReactNode }): JSX.Element {
  const [loading, setLoading] = useState(true);
  const [directory, setDirectory] = useState<Directory>({ rcpIdDirectory: {} });

  useEffect(() => {
    if (loading) {
      (async () => {
        const response = await axios.get('http://127.0.0.1:8081/dir');
        const directory = response.data;
        setDirectory(directory);
        setLoading(false);
      })();
    }
  }, [loading]);

  function getRcpById(rcpId: string): RCP | undefined {
    return directory.rcpIdDirectory[rcpId];
  }

  return (
    <DirectoryContext.Provider
      value={{
        directory,
        getRcpById
      }}
    >
      {loading ? <></> : children}
    </DirectoryContext.Provider>
  );
}
