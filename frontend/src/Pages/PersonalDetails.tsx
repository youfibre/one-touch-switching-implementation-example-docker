import { useEffect, useState } from "react";
import AddressDropdown from "../Components/AddressDropdown";
import Plan from "../Components/Plan";

type PersonalDetailsData = {
  firstName: string;
  lastName: string;
  email: string;
  uprn: number;
};

function PersonalDetails() {
  useEffect(() => {
    if (!localStorage.getItem('selectedPlan')) {
      document.location = '/';
    }
  }, []);

  const defaultState = JSON.stringify({
    firstName: '',
    lastName: '',
    email: '',
    uprn: 0
  });

  const [currentState, setCurrentState] = useState<PersonalDetailsData>(
    JSON.parse(localStorage.getItem('personalDetails') || defaultState) as PersonalDetailsData
  );

  const [firstName, setFirstName] = useState<string>(currentState.firstName);
  const [lastName, setLastName] = useState<string>(currentState.lastName);
  const [email, setEmail] = useState<string>(currentState.email);
  const [uprn, setUprn] = useState<number>(currentState.uprn);

  return (
    <>
      <h1 className="text-center mb-5">We need some details to set up your account.</h1>

      <div className="row">
        <div className="col-8">
          <div className="container">
            <div className="form-group mb-4">
              <label htmlFor="firstName">First Name</label>
              <input
                type="text"
                id="firstName"
                onChange={(e) => setFirstName(e.target.value)}
                value={firstName}
                placeholder="Enter your first name"
                className="form-control"
              />
            </div>

            <div className="form-group mb-4">
              <label htmlFor="lastName">Last Name</label>
              <input
                type="text"
                id="lastName"
                onChange={(e) => setLastName(e.target.value)}
                value={lastName}
                placeholder="Enter your last name"
                className="form-control"
              />
            </div>

            <div className="form-group mb-4">
              <label htmlFor="emailAddress">Email Address</label>
              <input
                type="email"
                id="emailAddress"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
                placeholder="Enter your email address"
                className="form-control"
              />
            </div>

            <div className="form-group mb-4">
              <AddressDropdown
                currentUprn={uprn}
                onChange={(uprn: number) => setUprn(uprn)}
              />
            </div>

            <div className="text-end">
              <button
                type="button"
                className="btn btn-primary"
                onClick={(e) => {
                  e.preventDefault();
                  localStorage.setItem('personalDetails', JSON.stringify({
                    ...currentState,
                    firstName,
                    lastName,
                    email,
                    uprn
                  }));
                  document.location = '/ots-init';
                }}
              >Continue</button>
            </div>
          </div>
        </div>
        
        <div className="col-4">
          <Plan plan={JSON.parse(localStorage.getItem('selectedPlan') || '')} withSelectButton={false} />
        </div>
      </div>
    </>
  );
}

export default PersonalDetails;
