import Plan from "../Components/Plan";
import { useAppStatus } from "../Context/AppStatusContext";
import { DummyPlan } from "../Types";

function SelectPlan() {
  const appStatus = useAppStatus();

  const plans: {[key: string]: DummyPlan} = {
    'plan_1': {
      name: 'Plan 1',
      speed: '150Mbps',
      price: 21.99
    },
    'plan_2': {
      name: 'Plan 2',
      speed: '500Mbps',
      price: 27.99
    },
    'plan_3': {
      name: 'Plan 3',
      speed: '920Mbps',
      price: 29.99
    }
  };

  return (
    <>
      <h1 className="text-center mb-5">Select your {appStatus.status.rcpName} package.</h1>
      <div className="container">
        <div className="row">
          {Object.keys(plans).map((planId: string) => {
            const plan = plans[planId];
            return (
              <div key={planId} className="col-4">
                <Plan plan={plan} withSelectButton={true} />
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
}

export default SelectPlan;
