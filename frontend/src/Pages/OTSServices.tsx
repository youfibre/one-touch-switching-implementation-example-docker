import { useEffect, useState } from "react";
import { useAppStatus } from "../Context/AppStatusContext";
import { useDirectory } from "../Context/DirectoryContext";

type OTSServicesState = {
  switchBroadband: boolean;
  switchVoice: boolean;
  voiceDn: string;
  voiceAction: 'cease' | 'port';
};

function OTSServices() {
  useEffect(() => {
    if (
      !localStorage.getItem('selectedPlan') ||
      !localStorage.getItem('personalDetails') ||
      !localStorage.getItem('currentRcpId')
    ) {
      document.location = '/';
    }
  }, []);

  const appStatus = useAppStatus();

  const defaultState = JSON.stringify({
    switchBroadband: true,
    switchVoice: false,
    voiceDn: '',
    voiceAction: 'cease'
  } as OTSServicesState);
  
  const [currentState] = useState<OTSServicesState>(
    JSON.parse(localStorage.getItem('otsServicesState') || defaultState)
  );

  const [switchBroadband, setSwitchBroadband] = useState<boolean>(currentState.switchBroadband);
  const [switchVoice, setSwitchVoice] = useState<boolean>(currentState.switchVoice);
  const [voiceDn, setVoiceDn] = useState<string>(currentState.voiceDn);
  const [voiceAction, setVoiceAction] = useState<'cease' | 'port'>(currentState.voiceAction);

  const directory = useDirectory();
  const rcp = directory.getRcpById(localStorage.getItem('currentRcpId') || '');

  return (
    <>
      <h1 className="text-center mb-5">Instant Switching - Current Services</h1>
      <div className="container">
        <p>
          We need to know which services you have with <strong>{rcp?.name}</strong> to allow us to find your account.
        </p>

        <div className="form-group mb-4">
          <label htmlFor="switchBroadband">
            Do you have a Broadband/Internet service with <strong>{rcp?.name}</strong> which you would like us to help switch to <strong>{appStatus.status.rcpName}</strong>?
          </label>
          <select
            id="switchBroadband"
            className="form-control"
            onChange={(e) => setSwitchBroadband(e.target.value === 'yes' ? true : false)}
            value={switchBroadband ? 'yes' : 'no'}
          >
            <option value="yes">Yes</option>
            <option value="no">No</option>
          </select>
        </div>

        <div className="form-group mb-4">
          <label htmlFor="switchVoice">
            Do you have a phone/VoIP service with <strong>{rcp?.name}</strong> which you would like us to help switch to <strong>{appStatus.status.rcpName}</strong>?
          </label>
          <select
            id="switchVoice"
            className="form-control"
            onChange={(e) => setSwitchVoice(e.target.value === 'yes' ? true : false)}
            value={switchVoice ? 'yes' : 'no'}
          >
            <option value="yes">Yes</option>
            <option value="no">No</option>
          </select>
        </div>

        {switchVoice ? <>
          <div className="form-group mb-4">
            <label htmlFor="voiceDn">
              Please provide us with the relevant phone number.
            </label>
            <input
              type="number"
              id="voiceDn"
              placeholder="01443000000"
              className="form-control"
              onChange={(e) => setVoiceDn(e.target.value)}
              value={voiceDn || ''}
            />
          </div>
        </> : <></>}

        {switchVoice && voiceDn ? <>
          <div className="form-group mb-4">
            <label htmlFor="voiceAction">
              Would you like to move your phone number to <strong>{appStatus.status.rcpName}</strong>, or cancel it?
            </label>
            <select
              id="voiceAction"
              className="form-control"
              onChange={(e) => setVoiceAction(e.target.value as 'cease' | 'port')}
              value={voiceAction}
            >
              <option value="cease">I do not wish to keep my number.</option>
              <option value="port">I would like to move my number to {appStatus.status.rcpName}.</option>
            </select>
          </div>
        </> : <></>}

        <div className="row">
          <div className="col">
            <button
              type="button"
              className="btn btn-secondary"
              onClick={() => document.location = '/ots-init'}
            >
              Back
            </button>
          </div>
          <div className="col text-end">
            <button
              type="button"
              className="btn btn-primary"
              onClick={(e) => {
                e.preventDefault();

                localStorage.setItem('otsServicesState', JSON.stringify({
                  ...currentState,
                  switchBroadband,
                  switchVoice,
                  voiceDn,
                  voiceAction
                }));

                if (!switchBroadband && !switchVoice) {
                  // If they have selected no services then we can assume they are no longer opted in.
                  localStorage.setItem('currentRcpId', 'not-opted-in');
                  document.location = '/direct-debit';
                } else {
                  document.location = '/ots-match-consent';
                }
              }}
            >Continue</button>
          </div>
        </div>
      </div>
    </>
  );
}

export default OTSServices;
