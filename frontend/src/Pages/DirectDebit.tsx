import { useEffect, useState } from "react";
import AddressDropdown from "../Components/AddressDropdown";
import Plan from "../Components/Plan";
import { useAppStatus } from "../Context/AppStatusContext";

function DirectDebit() {
  useEffect(() => {
    if (
      !localStorage.getItem('selectedPlan') ||
      !localStorage.getItem('personalDetails')
    ) {
      document.location = '/';
    }
  }, []);

  const appStatus = useAppStatus();
  const selectedPlan = JSON.parse(localStorage.getItem('selectedPlan') || '');
  const [accountHolderName, setAccountHolderName] = useState<string>('');
  const [sortCode, setSortCode] = useState<string>('');
  const [accountNumber, setAccountNumber] = useState<string>('');

  return (
    <>
      <h1 className="text-center mb-5">Please provide your bank account information to set up a Direct Debit.</h1>

      <div className="row">
        <div className="col-8">
          <div className="container">
            <div className="form-group mb-4">
              <label htmlFor="accountHolderName">Account Holder Name</label>
              <input
                type="text"
                id="accountHolderName"
                onChange={(e) => setAccountHolderName(e.target.value)}
                value={accountHolderName}
                placeholder="Account Holder Name"
                className="form-control"
              />
            </div>

            <div className="form-group mb-4">
              <label htmlFor="sortCode">Sort Code</label>
              <input
                type="email"
                id="sortCode"
                onChange={(e) => setSortCode(e.target.value)}
                value={sortCode}
                placeholder="XX-XX-XX"
                className="form-control"
              />
            </div>

            <div className="form-group mb-4">
              <label htmlFor="accountNumber">Account Number</label>
              <input
                type="text"
                id="accountNumber"
                onChange={(e) => setAccountNumber(e.target.value)}
                value={accountNumber}
                placeholder="Enter your 8-digit account number."
                className="form-control"
              />
            </div>

            <p className="text-center">
              <strong>{appStatus.status.rcpName}</strong> will set up a Direct Debit for £{selectedPlan.price} per month. By continuing, you confirm that you are the account holder or otherwise have authority to create Direct Debits for this account.
            </p>

            <div className="text-end">
              <button
                type="button"
                className="btn btn-primary"
                onClick={(e) => {
                  e.preventDefault();
                  document.location = '/completed';
                }}
              >Set up Direct Debit</button>
            </div>
          </div>
        </div>
        
        <div className="col-4">
          <Plan plan={selectedPlan} withSelectButton={false} />
        </div>
      </div>
    </>
  );
}

export default DirectDebit;
