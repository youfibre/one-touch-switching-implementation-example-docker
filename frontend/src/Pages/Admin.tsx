import axios from "axios";
import { useEffect, useState } from "react";
import { useAppStatus } from "../Context/AppStatusContext";

function Admin() {
  const appStatus = useAppStatus();
  const [fresh, setFresh] = useState<boolean>(false);
  const [refreshingIn, setRefreshingIn] = useState<number>(5);

  useEffect(() => {
    let timer = refreshingIn;
    const interval = setInterval(async () => {
      timer = timer-1;
      if (timer === 0) {
        timer = 5;
        setFresh(false);
      }
      setRefreshingIn(timer);
    }, 1000);

    return () => {
      clearInterval(interval);
    }
  }, []);

  useEffect(() => {
    if (!fresh) {
      appStatus.refreshAppStatus();
      setFresh(true);
      return;
    }
    console.log('Latest App Status', appStatus.status);
  }, [fresh]);

  async function installService(service: any) {
    await axios.patch('http://127.0.0.1:8081/install/' + service.serviceId);
    await appStatus.refreshAppStatus();
  }

  const switchOrderLabelClasses: {[key: string]: string} = {
    request: 'text-bg-warning',
    match: 'text-bg-info',
    order: 'text-bg-primary',
    completed: 'text-bg-success',
    cancelled: 'text-bg-secondary'
  };

  return (
    <>
      <h1 className="text-center mb-5">{appStatus.status.rcpName} Admin Dashboard</h1>
      <p className="text-center text-muted text-lead">Refreshing in {refreshingIn} seconds.</p>

      <div className="card card-default">
        <div className="card-header">Customers</div>
        <div className="card-body d-flex flex-column gap-4">
          {appStatus.status.customers.map((customer: any) => {
            return (
              <div className="card" key={customer.accountNumber}>
                <div className="card-header text-bg-secondary">{customer.accountNumber} - {customer.name}</div>
                <div className="card-body">
                  <div className="row mb-4">
                    <div className="col">
                      <div className="card">
                        <div className="card-body">
                          <p><strong>Contact Information</strong></p>
                          <p className="mb-0"><strong>Full Name:</strong> {customer.name}</p>
                          <p className="mb-0"><strong>Email Address:</strong> {customer.email}</p>
                        </div>
                      </div>
                    </div>

                    <div className="col">
                      <div className="card">
                        <div className="card-body">
                          <p><strong>Address</strong></p>
                          {customer.address.addressLines.map((line: string) => {
                            return (
                              <p className="mb-0" key={line}>{line},</p>
                            );
                          })}
                          <p className="mb-0">{customer.address.postTown},</p>
                          <p className="mb-0">{customer.address.postCode}</p>
                          <p><span className="badge text-bg-secondary">UPRN: {customer.uprn}</span></p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <h4 className="mt-5">Services</h4>
                  <div className="row">
                    {customer.services.map((svc: any) => {
                      return (
                        <div className="col-12" key={svc.serviceId}>
                          <div className="card">
                            <div className="card-body">
                              <p><strong>Service</strong></p>
                              <p className="mb-0"><strong>Service ID:</strong> {svc.serviceId}</p>
                              <p className="mb-0"><strong>Service Type:</strong> {svc.type}</p>
                              <p className="mb-0">
                                <strong>Status:</strong>
                                <span className={"ms-2 badge text-bg-" + (svc.status === 'installed' ? 'success' : 'danger')}>
                                  {svc.status}
                                </span>
                              </p>
                            </div>
                            {svc.status !== 'installed' ? (
                              <div className="card-footer text-end">
                                <button
                                  type="button"
                                  className="btn btn-success btn-sm"
                                  onClick={(e) => { installService(svc); }}
                                >Mark as Installed</button>
                              </div>
                            ) : <></>}
                          </div>
                        </div>
                      );
                    })}
                  </div>

                  <h4 className="mt-5">Switch Orders</h4>
                  <div className="row">
                    {appStatus.status.switchOrders.filter((so: any) => so.customer.accountNumber === customer.accountNumber).map((so: any) => {
                      return (
                        <div className="col-12" key={so.correlationId}>
                          <div className="card">
                            <div className="card-body">
                              <p><strong>Switch Order</strong></p>
                              <p className="mb-0">
                                <strong>Current Stage:</strong>
                                <span className={`ms-2 badge ${switchOrderLabelClasses[so.currentStage]}`}>
                                  {so.currentStage}
                                </span>
                              </p>
                              <p className="mb-0"><strong>Correlation ID:</strong> <kbd>{so.correlationId}</kbd></p>
                              <p className="mb-0"><strong>Gaining RCP:</strong> {so.gainingProviderRcpId}</p>
                              <p className="mb-0"><strong>Losing RCP:</strong> {so.losingProviderRcpId}</p>
                              <strong>Related Service IDs</strong>
                              <ul>
                                {so.switchMatchRequest.relatedServiceIds.map((sid: string) => {
                                  return <li key={sid}>{sid}</li>;
                                })}
                              </ul>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
}

export default Admin;
