import { useEffect, useState } from "react";
import ProviderDropdown from "../Components/ProviderDropdown";
import { useAppStatus } from "../Context/AppStatusContext";

function OTSInitiate() {
  const appStatus = useAppStatus();

  useEffect(() => {
    if (!localStorage.getItem('selectedPlan') || !localStorage.getItem('personalDetails')) {
      document.location = '/';
    }
  }, []);

  const [currentRcpId, setCurrentRcpId] = useState<string>(localStorage.getItem('currentRcpId') || 'not-opted-in');
  useEffect(() => {
    localStorage.setItem('currentRcpId', currentRcpId);
  }, [currentRcpId]);

  return (
    <>
      <h1 className="text-center mb-5">Instant Switching - Opt-In</h1>
      <div className="container">
        <p>
          <strong>{appStatus.status.rcpName}</strong> can help you switch from your existing provider through an opt-in process, at your discretion. If you would like to opt-in, select your current provider below.
        </p>

        <h3>How it works</h3>
        <ol>
          <li>You tell us who your current provider is</li>
          <li>You tell us which services you currently have with them</li>
          <li>We will perform a check with your current provider to see if your account supports instant switching</li>
          <li>Your current provider will email you details of the switching process</li>
          <li>We will present you with a list of options for cancelling or porting your existing services from your old provider to <strong>{appStatus.status.rcpName}</strong></li>
          <li>You will have your old connection until <strong>{appStatus.status.rcpName}</strong> have successfully installed your new service</li>
          <li>Once your new service is working, your old provider will close your account.</li>
        </ol>

        <div className="form-group mb-4">
          <ProviderDropdown
            currentRcpId={currentRcpId}
            onChange={(rcpId: string) => setCurrentRcpId(rcpId)}
          />
        </div>

        <div className="text-end">
          <button
            type="button"
            className="btn btn-primary"
            onClick={(e) => {
              e.preventDefault();

              if (currentRcpId === 'not-opted-in') {
                document.location = '/direct-debit';
              } else {
                document.location = '/ots-services';
              }
            }}
          >Continue</button>
        </div>
      </div>
    </>
  );
}

export default OTSInitiate;
