import axios from "axios";
import { useEffect, useState } from "react";
import { useAppStatus } from "../Context/AppStatusContext";
import { useDirectory } from "../Context/DirectoryContext";

type OTSServicesState = {
  switchBroadband: boolean;
  switchVoice: boolean;
  voiceDn?: string;
  voiceAction?: 'cease' | 'port';
};

function OTSPerformMatch() {
  useEffect(() => {
    if (
      !localStorage.getItem('selectedPlan') ||
      !localStorage.getItem('personalDetails') ||
      !localStorage.getItem('currentRcpId') ||
      !localStorage.getItem('otsServicesState') ||
      !localStorage.getItem('otsMatchConsent')
    ) {
      document.location = '/';
    }
  }, []);

  const [submitted, setSubmitted] = useState(false);
  const [switchMatchRequestCorrelationId, setSwitchMatchRequestCorrelationId] = useState<string>();
  const [failure, setFailure] = useState<any>(undefined);
  const appStatus = useAppStatus();

  useEffect(() => {
    if (!submitted) {
      setSubmitted(true);
      (async () => {
        const state = {
          selectedPlan: JSON.parse(localStorage.getItem('selectedPlan') || ''),
          personalDetails: JSON.parse(localStorage.getItem('personalDetails') || ''),
          currentRcpId: localStorage.getItem('currentRcpId'),
          otsServicesState: JSON.parse(localStorage.getItem('otsServicesState') || ''),
          otsMatchConsent: localStorage.getItem('otsMatchConsent')
        };

        const services = [];
        if (state.otsServicesState.switchBroadband) {
          services.push({
            provider: state.currentRcpId,
            type: 'IAS',
            action: 'cease'
          });
        }

        if (state.otsServicesState.switchVoice) {
          services.push({
            provider: state.currentRcpId,
            type: 'NBICS',
            phoneNumber: state.otsServicesState.voiceDn,
            action: state.otsServicesState.voiceAction
          });
        }

        const obj = {
          accountHolderName: `${state.personalDetails.firstName} ${state.personalDetails.lastName}`,
          accountHolderEmail: state.personalDetails.email,
          uprn: state.personalDetails.uprn,
          services
        };

        const response = await axios.post('http://127.0.0.1:8081/create-customer', obj);
        const correlationId = response.data.request.envelope.source.correlationID;

        // In reality, you'd use the customer's account to track this but in the essence of time...
        setSwitchMatchRequestCorrelationId(correlationId);
        await appStatus.refreshAppStatus();
      })();
    }
  }, [submitted, appStatus]);

  useEffect(() => {
    if (switchMatchRequestCorrelationId) {
      const switchOrder = appStatus.status.switchOrders.find((so: any) => {
        return so.correlationId === switchMatchRequestCorrelationId;
      });

      if (!switchOrder || !['match', 'cancelled'].includes(switchOrder.currentStage)) {
        setTimeout(async () => {
          await appStatus.refreshAppStatus();
        }, 5000);
        return;
      }

      if (switchOrder.currentStage === 'cancelled') {
        setFailure(switchOrder.switchMatchRequest.switchMatchFailure.residentialSwitchMatchFailure);
        return;
      }

      localStorage.setItem('switchOrder', JSON.stringify(switchOrder));
      document.location = '/ots-match-results';
    }
  }, [appStatus]);

  const directory = useDirectory();
  const rcp = directory.getRcpById(localStorage.getItem('currentRcpId') || '');

  return (failure ? (
    <>
      <h1 className="text-center mb-5">Instant Switching - Failure</h1>
      <div className="container text-center">
        <p>We couldn't locate your account with <strong>{rcp?.name}</strong>.</p>
        <div className="card card-default my-4">
          <div className="card-body">
            <p className="mb-0"><strong>Reason:</strong> {failure.faultText} ({failure.faultCode})</p>
            {failure.faultElement ? <p className="mt-3 mb-0"><strong>Problem with:</strong> {failure.faultElement}</p> : <></>}
          </div>
        </div>
        <p>Please carefully check the personal details you have provided.</p>
        <button type="button" onClick={() => document.location = '/personal-details'} className="btn btn-primary">Go back to Personal Details</button>
      </div>
    </>
  ) : (
    <>
      <h1 className="text-center mb-5">Instant Switching - Checking...</h1>
      <div className="container text-center">
        <p>
          <span className="spinner-border spinner-border-sm d-inline-block me-3" role="status">
            <span className="visually-hidden">Loading...</span>
          </span>
          Please wait while <strong>{appStatus.status.rcpName}</strong> conducts some checks with <strong>{rcp?.name}</strong> using the details you provided. This should only take a moment.
        </p>
      </div>
    </>
  ));
}

export default OTSPerformMatch;
