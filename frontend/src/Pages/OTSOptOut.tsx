import { useEffect, useState } from "react";
import ProviderDropdown from "../Components/ProviderDropdown";
import { useAppStatus } from "../Context/AppStatusContext";

function OTSOptOut() {
  const appStatus = useAppStatus();

  useEffect(() => {
    if (
      !localStorage.getItem('selectedPlan') ||
      !localStorage.getItem('personalDetails') ||
      !localStorage.getItem('currentRcpId') ||
      !localStorage.getItem('otsServicesState') ||
      !localStorage.getItem('otsMatchConsent') ||
      !localStorage.getItem('switchOrder')
    ) {
      document.location = '/';
    }
  }, []);

  return (
    <>
      <h1 className="text-center mb-5">Instant Switching - Opt-Out</h1>
      <div className="container">
        <p className="mb-5">
          We're sorry none of the options are suitable for you. You may continue joining {appStatus.status.rcpName}, but we are unable to provide additional switching options. You will need to contact your current provider to cancel your services.
        </p>

        <div className="text-center">
          <div className="d-block mb-2">
            <button
              type="button"
              className="btn btn-primary"
              onClick={(e) => {
                e.preventDefault();
                localStorage.removeItem('currentRcpId');
                localStorage.removeItem('otsServicesState');
                localStorage.removeItem('otsMatchConsent');
                localStorage.removeItem('switchOrder');
                localStorage.removeItem('selectedMatchResult');
                localStorage.removeItem('otsExpressConsent');
                document.location = '/direct-debit';
              }}
            >Continue without Instant Switching</button>
          </div>

          <button
            type="button"
            className="btn btn-secondary"
            onClick={(e) => {
              e.preventDefault();
              document.location = '/ots-match-results';
            }}
          >Go back to Match Results</button>
        </div>
      </div>
    </>
  );
}

export default OTSOptOut;
