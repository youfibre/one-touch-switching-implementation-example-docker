

import { useEffect } from "react";
import { useDirectory } from "../Context/DirectoryContext";

function OTSMatchResults() {
  useEffect(() => {
    if (
      !localStorage.getItem('selectedPlan') ||
      !localStorage.getItem('personalDetails') ||
      !localStorage.getItem('currentRcpId') ||
      !localStorage.getItem('otsServicesState') ||
      !localStorage.getItem('otsMatchConsent') ||
      !localStorage.getItem('switchOrder')
    ) {
      document.location = '/';
    }
  }, []);

  const directory = useDirectory();
  const rcp = directory.getRcpById(localStorage.getItem('currentRcpId') || '');
  const switchOrder = JSON.parse(localStorage.getItem('switchOrder') || '{}');
  const confirmation = switchOrder.switchMatchRequest.switchMatchConfirmation.residentialSwitchMatchConfirmation;
  const matchResults = [
    confirmation.matchResult,
    ...(confirmation.alternativeSwitchOrders || []).map((mr: any) => mr.matchResult)
  ];

  function selectOption(matchResult: any) {
    localStorage.setItem('selectedMatchResult', JSON.stringify(matchResult));
    document.location = '/ots-match-result';
  }

  return (
    <>
      <h1 className="text-center mb-5">Instant Switching - Match Results</h1>
      <div className="container">
        <p>
          We have managed to locate your account with <strong>{rcp?.name || 'your existing provider'}</strong> and have been provided with the following options related to your account.
        </p>
        <p>
          Note that <strong>{rcp?.name || 'your existing provider'}</strong> has emailed you a copy of the implications for each of these options, including any Early Termination Charges or other impacts.
        </p>

        {matchResults.map((mr: any) => {
          return (
            <div className="card card-default mb-4" key={mr.switchOrderReference}>
              <div className="card-header">
                <div className="d-flex align-items-center">
                  <span className="me-auto">Option #{matchResults.indexOf(mr)+1}</span>
                  {matchResults.indexOf(mr) === 0 ? <span className="ml-5 badge text-bg-warning">Recommended</span> : <></>}
                </div>
              </div>
              <div className="card-body">
                <p>Affected Services:</p>
                <ul>
                  {mr.services.map((service: any) => {
                    return (<li key={mr.services.indexOf(service)}>
                      {service.serviceType === 'IAS' ? 'Broadband Service' : 'Phone Service'}
                      {service.switchAction.startsWith('OptionTo') ? (
                        <span className="badge text-bg-info ms-3">{service.switchAction.replace('OptionTo', 'Option to ')}</span>
                      ) : ''}
                      {(service.serviceIdentifiers || []).length > 0 ? (<>
                        <br /><strong>Service Identifiers:</strong>
                        <ul>
                          {service.serviceIdentifiers.map((si: any) => {
                            return (<li key={service.serviceIdentifiers.indexOf(si)}><strong>{si.identifierType}</strong> is <strong>{si.identifier}</strong></li>);
                          })}
                        </ul>
                      </>) : <></>}
                    </li>);
                  })}
                </ul>
              </div>
              <div className="card-footer text-end">
                <button
                  type="button"
                  className="btn btn-primary btn-sm"
                  onClick={() => { selectOption(mr); }}
                >Proceed with Option #{matchResults.indexOf(mr)+1} &gt;</button>
              </div>
            </div>
          );
        })}

        <div className="text-center">
          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => { document.location = '/ots-opt-out'; }}
          >None of these options are suitable for me.</button>
        </div>
      </div>
    </>
  );
}

export default OTSMatchResults;
