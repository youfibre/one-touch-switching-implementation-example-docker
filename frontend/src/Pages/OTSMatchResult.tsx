import { useEffect, useState } from "react";
import { useAppStatus } from "../Context/AppStatusContext";
import { useDirectory } from "../Context/DirectoryContext";

function OTSMatchResult() {
  useEffect(() => {
    if (
      !localStorage.getItem('selectedPlan') ||
      !localStorage.getItem('personalDetails') ||
      !localStorage.getItem('currentRcpId') ||
      !localStorage.getItem('otsServicesState') ||
      !localStorage.getItem('otsMatchConsent') ||
      !localStorage.getItem('switchOrder') ||
      !localStorage.getItem('selectedMatchResult')
    ) {
      document.location = '/';
    }
  }, []);

  const appStatus = useAppStatus();
  const directory = useDirectory();
  const personalDetails = JSON.parse(localStorage.getItem('personalDetails') || '');
  const rcp = directory.getRcpById(localStorage.getItem('currentRcpId') || '');
  const switchOrder = JSON.parse(localStorage.getItem('switchOrder') || '{}');
  const confirmation = switchOrder.switchMatchRequest.switchMatchConfirmation.residentialSwitchMatchConfirmation;
  const matchResults = [
    confirmation.matchResult,
    ...(confirmation.alternativeSwitchOrders || []).map((mr: any) => mr.matchResult)
  ];
  const matchResult = JSON.parse(localStorage.getItem('selectedMatchResult') || '');
  const optionNumber = matchResults.indexOf(matchResults.find((mr: any) => mr.switchOrderReference === matchResult.switchOrderReference))+1;
  const [expressConsent, setExpressConsent] = useState<boolean>(false);

  function handleContinue(e: any) {
    e.preventDefault();
    localStorage.setItem('otsExpressConsent', `${expressConsent}`);
    document.location = '/ots-finalise';
  }

  return (
    <>
      <h1 className="text-center mb-5">Instant Switching - Selected Option</h1>
      <div className="container">
        <p>
          You have selected Option #{optionNumber}. Please ensure that you have read the documentation provided to you by <strong>{rcp?.name}</strong> via email so that you do not incur any unexpected fees from your existing provider.
        </p>
        
        <div className="card card-default mb-4">
          <div className="card-header">
            <div className="d-flex align-items-center">
              <span className="me-auto">Option #{optionNumber}</span>
              {matchResults.indexOf(matchResult) === 0 ? <span className="ml-5 badge text-bg-warning">Recommended</span> : <></>}
            </div>
          </div>
          <div className="card-body">
            <p>Affected Services:</p>
            <ul>
              {matchResult.services.map((service: any) => {
                return (<li key={matchResult.services.indexOf(service)}>
                  {service.serviceType === 'IAS' ? 'Broadband Service' : 'Phone Service'}
                  {service.switchAction.startsWith('OptionTo') ? (
                    <span className="badge text-bg-info ms-3">{service.switchAction.replace('OptionTo', 'Option to ')}</span>
                  ) : ''}
                  {(service.serviceIdentifiers || []).length > 0 ? (<>
                    <br /><strong>Service Identifiers:</strong>
                    <ul>
                      {service.serviceIdentifiers.map((si: any) => {
                        return (<li key={service.serviceIdentifiers.indexOf(si)}><strong>{si.identifierType}</strong> is <strong>{si.identifier}</strong></li>);
                      })}
                    </ul>
                  </>) : <></>}
                </li>);
              })}
            </ul>
          </div>
        </div>

        <div className="form-check mb-4">
          <input
            className="form-check-input"
            type="checkbox"
            id="otsExpressConsent"
            checked={expressConsent}
            onChange={(e) => { setExpressConsent(e.target.checked); }} />
          <label className="form-check-label" htmlFor="otsExpressConsent">
            I, <strong>{personalDetails.firstName} {personalDetails.lastName}</strong>, have read all of the information provided to me by my current supplier (<strong>{rcp?.name}</strong>), have understood it, and grant express consent for <strong>{appStatus.status.rcpName}</strong> to act on my behalf in carrying out changes to the services above inline with the implications communicated by my current supplier.
          </label>
        </div>

        <div className="text-center">
          <div className="d-block mb-3">
            <button
              type="button"
              className="btn btn-primary btn-lg"
              disabled={!expressConsent}
              onClick={handleContinue}
            >Continue</button>
          </div>

          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => { document.location = '/ots-match-results'; }}
          >Go back</button>
        </div>
      </div>
    </>
  );
}

export default OTSMatchResult;
