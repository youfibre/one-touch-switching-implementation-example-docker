import { useEffect } from "react";
import Plan from "../Components/Plan";
import { useAppStatus } from "../Context/AppStatusContext";

function Completed() {
  useEffect(() => {
    if (
      !localStorage.getItem('selectedPlan') ||
      !localStorage.getItem('personalDetails')
    ) {
      document.location = '/';
    }
  }, []);

  const appStatus = useAppStatus();
  const personalDetails = JSON.parse(localStorage.getItem('personalDetails') || '');

  return (
    <div className="text-center">
      <h1 className="mb-5">Welcome Aboard!</h1>
      <p>You're all set, {personalDetails.firstName}, thanks for joining the {appStatus.status.rcpName} family!</p>
      <p>We will be in touch with more details about your connection via email, so keep an eye out!</p>
      <p className="mt-5">Here's a reminder of your connection:</p>
      <div className="row justify-content-center">
        <div className="col-4">
          <Plan plan={JSON.parse(localStorage.getItem('selectedPlan') || '')} withSelectButton={false} />
          <small className="text-muted mt-2 d-block">(payment via Direct Debit)</small>
        </div>
      </div>
    </div>
  );
}

export default Completed;
