import { useEffect, useState } from "react";
import ProviderDropdown from "../Components/ProviderDropdown";
import { useDirectory } from "../Context/DirectoryContext";

type OTSServicesState = {
  switchBroadband: boolean;
  switchVoice: boolean;
  voiceDn?: string;
  voiceAction?: 'cease' | 'port';
};

function OTSMatchConsent() {
  useEffect(() => {
    if (
      !localStorage.getItem('selectedPlan') ||
      !localStorage.getItem('personalDetails') ||
      !localStorage.getItem('currentRcpId') ||
      !localStorage.getItem('otsServicesState')
    ) {
      document.location = '/';
    }
  }, []);

  const defaultState = JSON.stringify({
    switchBroadband: true,
    switchVoice: false
  });
  
  const [currentState] = useState<OTSServicesState>(
    JSON.parse(localStorage.getItem('otsServicesState') || defaultState)
  );

  const directory = useDirectory();
  const rcp = directory.getRcpById(localStorage.getItem('currentRcpId') || '');

  return (
    <>
      <h1 className="text-center mb-5">Instant Switching - Match Consent</h1>
      <div className="container">
        <p>
          Thank you for providing your information. Before we run some checks, we need you to confirm the details you have provided.
        </p>

        <h3>Information Provided</h3>
        <ol>
          {currentState.switchBroadband ? (
            <li>
              You have a Broadband service with <strong>{rcp?.name}</strong> and you would like us to conduct a check with <strong>{rcp?.name}</strong> to see if we can <strong>cancel</strong> your service.
            </li>
          ) : <></>}

          {currentState.switchVoice ? (
            <li>
              You have a Phone service with <strong>{rcp?.name}</strong> with a phone number of <strong>{currentState.voiceDn}</strong>, and you would like us to conduct a check with <strong>{rcp?.name}</strong> to see if we can <strong>{currentState.voiceAction === 'port' ? 'move' : 'cancel'}</strong> your service.
            </li>
          ): <></>}

          <li>
            You do not yet give consent to make any changes, but only to check which options are available.
          </li>
        </ol>

        <div className="row">
          <div className="col">
            <button
              type="button"
              className="btn btn-secondary"
              onClick={() => document.location = '/ots-services'}
            >
              Go back, I spotted a problem.
            </button>
          </div>
          <div className="col text-end">
            <button
              type="button"
              className="btn btn-primary"
              onClick={(e) => {
                e.preventDefault();
                localStorage.setItem('otsMatchConsent', 'consent-given');
                document.location = '/ots-perform-match';
              }}
            >Yes, this looks perfect.</button>
          </div>
        </div>
      </div>
    </>
  );
}

export default OTSMatchConsent;
