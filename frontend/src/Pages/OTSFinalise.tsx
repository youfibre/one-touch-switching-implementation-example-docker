import axios from "axios";
import { useEffect, useState } from "react";
import { useAppStatus } from "../Context/AppStatusContext";
import { useDirectory } from "../Context/DirectoryContext";

function OTSFinalise() {
  useEffect(() => {
    if (
      !localStorage.getItem('selectedPlan') ||
      !localStorage.getItem('personalDetails') ||
      !localStorage.getItem('currentRcpId') ||
      !localStorage.getItem('otsServicesState') ||
      !localStorage.getItem('otsMatchConsent') ||
      !localStorage.getItem('switchOrder') ||
      !localStorage.getItem('selectedMatchResult') ||
      !localStorage.getItem('otsExpressConsent')
    ) {
      document.location = '/';
    }
  }, []);

  const appStatus = useAppStatus();
  const [submitted, setSubmitted] = useState(false);
  const switchOrder = JSON.parse(localStorage.getItem('switchOrder') || '');
  const matchResult = JSON.parse(localStorage.getItem('selectedMatchResult') || '');

  useEffect(() => {
    if (!submitted) {
      setSubmitted(true);
      (async () => {
        const obj = {
          correlationId: switchOrder.correlationId,
          switchOrderReference: matchResult.switchOrderReference
        };

        await axios.post('http://127.0.0.1:8081/place-switch-order', obj);
        document.location = '/direct-debit';
      })();
    }
  }, [submitted]);

  const directory = useDirectory();
  const rcp = directory.getRcpById(localStorage.getItem('currentRcpId') || '');

  return (
    <>
      <h1 className="text-center mb-5">Instant Switching - Informing {rcp?.name}...</h1>
      <div className="container text-center">
        <p>
          <span className="spinner-border spinner-border-sm d-inline-block me-3" role="status">
            <span className="visually-hidden">Loading...</span>
          </span>
          Please wait while <strong>{appStatus.status.rcpName}</strong> informs <strong>{rcp?.name}</strong> of your intentions. This should only take a moment.
        </p>
      </div>
    </>
  );
}

export default OTSFinalise;
