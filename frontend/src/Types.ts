export type DummyPlan = {
  name: string;
  speed: string;
  price: number;
};

export type Address = {
  uprn: number,
  address: {
    addressLines: string[],
    postTown: string,
    postCode: string
  }
};

export type RCP = {
  identity: string;
  name: string;
  email: string;
  publicKey?: string;
};

export type Directory = {
  rcpIdDirectory: {
    [key: string]: RCP
  }
};