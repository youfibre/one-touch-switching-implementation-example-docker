import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { AppStatusProvider } from './Context/AppStatusContext';
import { DirectoryProvider } from './Context/DirectoryContext';
import Admin from './Pages/Admin';
import Completed from './Pages/Completed';
import DirectDebit from './Pages/DirectDebit';
import OTSFinalise from './Pages/OTSFinalise';
import OTSInitiate from './Pages/OTSInitiate';
import OTSMatchConsent from './Pages/OTSMatchConsent';
import OTSMatchResult from './Pages/OTSMatchResult';
import OTSMatchResults from './Pages/OTSMatchResults';
import OTSOptOut from './Pages/OTSOptOut';
import OTSPerformMatch from './Pages/OTSPerformMatch';
import OTSServices from './Pages/OTSServices';
import PersonalDetails from './Pages/PersonalDetails';
import SelectPlan from './Pages/SelectPlan';

function App() {
  return (
    <div className="container py-5">
      <div className="row justify-content-center">
        <div className="col-10">
          <AppStatusProvider>
            <DirectoryProvider>
              <BrowserRouter>
                <Routes>
                  <Route path='/' element={<SelectPlan />} />
                  <Route path='/personal-details' element={<PersonalDetails />} />

                  {/* OTS Process */}
                  <Route path='/ots-init' element={<OTSInitiate />} />
                  <Route path='/ots-services' element={<OTSServices />} />
                  <Route path='/ots-match-consent' element={<OTSMatchConsent />} />
                  <Route path='/ots-perform-match' element={<OTSPerformMatch />} />
                  <Route path='/ots-match-results' element={<OTSMatchResults />} />
                  <Route path='/ots-match-result' element={<OTSMatchResult />} />
                  <Route path='/ots-opt-out' element={<OTSOptOut />} />
                  <Route path='/ots-finalise' element={<OTSFinalise />} />
                  {/* End of OTS Process */}

                  <Route path='/direct-debit' element={<DirectDebit />} />
                  <Route path='/completed' element={<Completed />} />

                  {/* Admin */}
                  <Route path='/admin' element={<Admin />} />
                </Routes>
              </BrowserRouter>
            </DirectoryProvider>
          </AppStatusProvider>
        </div>
      </div>
    </div>
  );
}

export default App;
