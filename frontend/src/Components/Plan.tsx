import { DummyPlan } from "../Types";

function Plan(props: {
  plan: DummyPlan;
  withSelectButton: boolean;
}) {
  function selectPlan() {
    localStorage.setItem('selectedPlan', JSON.stringify(props.plan));
    document.location = '/personal-details';
  }

  return (
    <div className="card text-center">
      <div className="card-header">
        <h3 className="mb-0">{props.plan.name}</h3>
      </div>
      <div className="card-body">
        <p>{props.plan.speed} avg. upload/download speed</p>
        <p className="mb-0"><strong>£{props.plan.price} per month</strong></p>
      </div>
      {props.withSelectButton ? (<div className="card-footer">
        <button type="button" className="btn btn-primary" onClick={() => selectPlan()}>Select</button>
      </div>) : <></>}
    </div>
  )
}

export default Plan;