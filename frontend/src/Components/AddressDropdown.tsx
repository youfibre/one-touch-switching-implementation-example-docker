import axios from "axios";
import { useEffect, useState } from "react";
import { useAppStatus } from "../Context/AppStatusContext";
import { Address } from "../Types";

function AddressDropdown(props: {
  currentUprn: number,
  onChange: (uprn: number) => void
}) {
  const appStatus = useAppStatus();
  const [addresses, setAddresses] = useState<Address[]>();

  useEffect(() => {
    setAddresses(appStatus.status.addresses);
    if (!props.currentUprn || props.currentUprn === 0) {
      props.onChange(appStatus.status.addresses[0].uprn);
    }
  }, []);

  return (addresses ? (<>
    <label htmlFor="address">Address</label>
    <select
      id="address"
      className="form-control"
      onChange={(e) => {console.log(e); props.onChange(parseInt(e.target.value));}}
      value={props.currentUprn}
    >
      {addresses.map((address: Address) => {
        return (
          <option value={address.uprn} key={address.uprn}>{[
            ...address.address.addressLines,
            address.address.postTown,
            address.address.postCode
          ].join(', ')}</option>
        );
      })}
    </select>
  </>) : <></>);
}

export default AddressDropdown;