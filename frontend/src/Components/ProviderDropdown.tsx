import { useAppStatus } from "../Context/AppStatusContext";
import { useDirectory } from "../Context/DirectoryContext";

function ProviderDropdown(props: {
  currentRcpId: string,
  onChange: (rcpId: string) => void
}) {
  const appStatus = useAppStatus();
  const directory = useDirectory();

  return (directory ? (<>
    <label htmlFor="provider">Current Provider</label>
    <select
      id="provider"
      className="form-control"
      onChange={(e) => props.onChange(e.target.value)}
      value={props.currentRcpId}
    >
      <option value='not-opted-in' key='not-opted-in'>I do not wish to opt-in.</option>
      {Object.keys(directory.directory.rcpIdDirectory).map((rcpId: string) => {
        if (rcpId === appStatus.status.rcpId) return;
        const provider = directory.directory.rcpIdDirectory[rcpId];
        return (
          <option value={rcpId} key={rcpId}>{provider.name}</option>
        );
      })}
    </select>
  </>) : <></>);
}

export default ProviderDropdown;