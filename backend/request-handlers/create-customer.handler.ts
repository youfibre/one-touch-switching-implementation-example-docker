import { ResidentialSwitchMatchRequest } from "@youfibre/otsc";
import { BroadbandService, Customer, VoiceService } from "data/customer-repository.class";
import { SwitchOrder } from "data/switch-order-repository.class";
import { Request, Response } from "express";

interface SwitchRequestDto {
  accountHolderName: string;
  accountHolderEmail: string;
  uprn: number;
  services: {
    provider: string;
    type: string;
    action: string;
    phoneNumber?: string;
  }[];
}

export async function handleCreateCustomerFormSubmission(req: Request, res: Response) {
  const body = req.body as SwitchRequestDto;

  const address = this.repositories.addressRepository.findByUprn(body.uprn);
  if (!address) {
    return res.send({
      error: true,
      status: 'Invalid UPRN specified.'
    });
  }

  // First we register the customer with the GP
  const newCustomer: Customer = {
    accountNumber: `ACC${new Date().getUTCMilliseconds()}`,
    name: body.accountHolderName,
    email: body.accountHolderEmail,
    uprn: body.uprn,
    address: address,
    services: body.services.map((s) => {
      // In this example, we simply take whatever services they requested to switch and convert them into
      // the GP's services. In reality, the customer is likely to select a product the GP offers
      // and then select what they want to terminate which has been provided from the LP's side.

      if (s.type === 'IAS') {
        return {
          serviceId: `SVC${new Date().getTime()}${Math.random()}`,
          status: 'ordered',
          type: s.type
        } as BroadbandService;
      }
      
      return {
        serviceId: `SVC${new Date().getTime()}${Math.random()}`,
        status: 'ordered',
        type: s.type,
        phoneNumber: s.phoneNumber
      } as VoiceService;
    }),
    // activeSwitchRequests: []
  };

  this.repositories.customerRepository.add(newCustomer);

  // Form the request to determine which services are available on the LP's side
  const rcpId = body.services[0].provider;
  const request = {
    envelope: {
      source: {
        type: 'RCPID',
        identity: this.rcpId,
        correlationID: `${new Date().getTime()}-${newCustomer.accountNumber}`
      },
      destination: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: ''
      }
    },
  
    residentialSwitchMatchRequest: {
      grcpBrandName: this.rcpName,
      name: newCustomer.name,
      // account: '0003316563216',
      uprn: body.uprn,
      address: address,
      services: req.body.services.map((s: any) => {
        if (s.type === 'IAS') {
          return {
            serviceType: 'IAS',
            action: 'cease'
          };
        } else {
          return {
            serviceType: 'NBICS',
            serviceIdentifierType: 'DN',
            serviceIdentifier: s.phoneNumber,
            action: s.action
          };
        }
      })
    }
  } as ResidentialSwitchMatchRequest;

  // Track the Switch Order in the 'request' state on the GP's system
  const switchOrder: SwitchOrder = {
    currentStage: 'request',
    switchOrderReference: '',
    correlationId: request.envelope.source.correlationID,
    customer: newCustomer,
    switchMatchRequest: { request, relatedServiceIds: newCustomer.services.map((s) => s.serviceId) },
    losingProviderRcpId: request.envelope.destination.identity,
    gainingProviderRcpId: request.envelope.source.identity
  };
  this.repositories.switchOrderRepository.add(switchOrder);

  // Send the request onto the LP
  await this.client.sendMessage(request);

  res.send({
    body,
    request
  });
}