import { ResidentialSwitchOrderTrigger } from "@youfibre/otsc";
import { Customer, Service } from "data/customer-repository.class";
import { SwitchOrder } from "data/switch-order-repository.class";
import { Request, Response } from "express";
import * as nodemailer from 'nodemailer';

export async function handleInstallServiceFormSubmission(req: Request, res: Response) {
  const customer: Customer = this.repositories.customerRepository.findByServiceId(req.params.serviceId);
  const service = customer.services.find((s: Service) => s.serviceId === req.params.serviceId);
  service.status = 'installed';
  
  const transport = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: parseInt(process.env.SMTP_PORT),
    auth: {
      user: process.env.SMTP_USERNAME,
      pass: process.env.SMTP_PASSWORD
    }
  });
  
  await transport.sendMail({
    subject: `Your ${process.env.GAINING_FROM_NAME} ${service.type === 'IAS' ? 'Broadband' : 'Phone'} Service has been installed!`,
    to: {
      address: customer.email,
      name: customer.name
    },
    from: {
      address: process.env.GAINING_FROM_EMAIL,
      name: process.env.GAINING_FROM_NAME
    },
    html: `
      <h1>${customer.name}, Your Service is Installed!</h1>
      <p>We're happy to confirm that your ${service.type === 'IAS' ? 'Broadband' : 'Phone'} Service is now live!</p>
      <p>If you need any help with your new service, please contact us on 0800 123 1234.</p>
    `
  });

  // Get all the switchOrders on the account which are currently in order state to see if we can trigger them yet
  const switchOrders: SwitchOrder[] = this.repositories.switchOrderRepository.findByCustomer(customer).filter(
    (so: SwitchOrder) => so.currentStage === 'order'
  );

  if (switchOrders.length > 0) {
    // We know we can trigger switchOrders if all services are installed
    for (const switchOrder of switchOrders) {
      if (switchOrder.switchMatchRequest.relatedServiceIds.some((sid: string) => {
        return customer.services.some((svc: Service) => svc.serviceId === sid && svc.status === 'ordered');
      })) {
        continue;
      }

      await this.client.sendMessage({
        envelope: switchOrder.switchMatchRequest.request.envelope,
        residentialSwitchOrderTrigger: {
          switchOrderReference: switchOrder.switchOrderReference
        }
      } as ResidentialSwitchOrderTrigger);
    }
  }

  res.send({
    failure: false
  });
}