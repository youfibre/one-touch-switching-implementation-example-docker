import { ResidentialSwitchOrder } from "@youfibre/otsc";
import { SwitchOrder } from "data/switch-order-repository.class";
import { Request, Response } from "express";
import * as nodemailer from 'nodemailer';

interface PlaceSwitchOrderDto {
  correlationId: string;
  switchOrderReference: string;
}

export async function handlePlaceSwitchOrderFormSubmission(req: Request, res: Response) {
  const body = req.body as PlaceSwitchOrderDto;

  // Locate the SwitchMatchResult
  const switchOrder = this.repositories.switchOrderRepository.findByCorrelationId(body.correlationId) as SwitchOrder;
  if (!switchOrder) {
    console.error(`🚨 Could not locate related Switch Order by Correlation ID during /place-switch-order form submission: ${body.correlationId}\n\n`);
    return res.send({
      failed: true,
      status: `Could not locate related Switch Order by Correlation ID: ${body.correlationId}`
    });
  }

  // Making an assumption of switching in a week but it could be any future date the service may be installed by
  const now = new Date();
  const switchDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 7);
  
  switchOrder.switchDate = switchDate;
  switchOrder.switchOrderReference = body.switchOrderReference;

  // Send the request onto the LP
  const finalSwitchOrder = {
    envelope: {
      source: switchOrder.switchMatchRequest.switchMatchConfirmation.envelope.destination,
      destination: switchOrder.switchMatchRequest.switchMatchConfirmation.envelope.source
    },
    residentialSwitchOrder: {
      plannedSwitchDate: switchDate.toISOString().split('T')[0],
      switchOrderReference: body.switchOrderReference
    }
  } as ResidentialSwitchOrder;
  await this.client.sendMessage(finalSwitchOrder);
  
  const transport = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: parseInt(process.env.SMTP_PORT),
    auth: {
      user: process.env.SMTP_USERNAME,
      pass: process.env.SMTP_PASSWORD
    }
  });
  
  await transport.sendMail({
    subject: `Welcome to ${process.env.GAINING_FROM_NAME}!`,
    to: {
      address: switchOrder.customer.email,
      name: switchOrder.customer.name
    },
    from: {
      address: process.env.GAINING_FROM_EMAIL,
      name: process.env.GAINING_FROM_NAME
    },
    html: `
      <h1>Welcome aboard, ${switchOrder.customer.name}!</h1>
      <p>We've let your current provider know that you're switching to us, and we've also let them know what you wanted to do with your existing services.</p>
      <hr />
      <p>Remember: you keep your existing services until you're fully installed with us!</p>
      <hr />
      <p>You will receive follow-up communications shortly about your installation and anything else we may need from you, so keep an eye out!</p>
      <p>If you need any help with your new account, please contact us on 0800 123 1234.</p>
    `
  });

  res.send({
    body,
    finalSwitchOrder
  });
}