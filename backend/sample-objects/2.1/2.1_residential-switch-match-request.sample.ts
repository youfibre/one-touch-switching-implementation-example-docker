import { ResidentialSwitchMatchRequest } from '@youfibre/otsc';

export default {
  envelope: {
    source: {
      type: 'RCPID',
      identity: 'RCP001',
      correlationID: 'XYZ987'
    },
    destination: {
      type: 'RCPID',
      identity: 'RCP002',
      correlationID: 'XYZ123'
    }
  },

  residentialSwitchMatchRequest: {
    grcpBrandName: 'YouFibre Limited',
    name: 'Miggins',
    account: '0003316563216',
    uprn: 12345,
    address: {
      addressLines: ['Flat 1', 'Rose Cottage', '22 Cheshunt Mews', 'Cypress Street', 'Tyre Industrial Estate', 'Blnatyre'],
      postTown: 'Glasgow',
      postCode: 'SW1P 3UX'
    },
    services: [
      {
        serviceType: 'IAS',
        action: 'cease'
      },
      {
        serviceType: 'NBICS',
        serviceIdentifierType: 'DN',
        serviceIdentifier: '0101111222',
        action: 'port'
      }
    ]
  }
} as ResidentialSwitchMatchRequest;