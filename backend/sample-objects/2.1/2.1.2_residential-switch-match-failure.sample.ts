import { ResidentialSwitchMatchFailure } from '@youfibre/otsc';

export default {
  envelope: {
    source: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ987'
    },
    destination: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ123'
    }
  },

  residentialSwitchMatchFailure: {
    faultCode: '101',
    faultText: 'Address not found',
    faultElement: 'postCode',
    faultElementValue: '',
    address: {
      addressLines: ['Flat 1', 'Rose Cottage', '22 Cheshunt Mews', 'Cypress Street', 'Tyre Industrial Estate', 'Blnatyre'],
      postTown: 'Glasgow',
      postCode: 'SW1P 3UX'
    }
  }
} as ResidentialSwitchMatchFailure;