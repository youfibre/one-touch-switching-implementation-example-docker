import { ResidentialSwitchOrderTriggerConfirmation } from '@youfibre/otsc';

export default {
  envelope: {
    source: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ987'
    },
    destination: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ123'
    }
  },

  residentialSwitchOrderTriggerConfirmation: {
    status: 'triggered'
  }
} as ResidentialSwitchOrderTriggerConfirmation;