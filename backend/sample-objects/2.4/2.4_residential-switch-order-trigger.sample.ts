import { ResidentialSwitchOrderTrigger } from '@youfibre/otsc';

export default {
  envelope: {
    source: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ987'
    },
    destination: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ123'
    }
  },

  residentialSwitchOrderTrigger: {
    switchOrderReference: '123e4567-e89b-12d3-a456-426614174000'
  }
} as ResidentialSwitchOrderTrigger;