import { ResidentialSwitchOrderUpdateConfirmation } from '@youfibre/otsc';

export default {
  envelope: {
    source: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ987'
    },
    destination: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ123'
    }
  },

  residentialSwitchOrderUpdateConfirmation: {
    status: 'updated'
  }
} as ResidentialSwitchOrderUpdateConfirmation;