import { ResidentialSwitchOrderCancellationFailure } from '@youfibre/otsc';

export default {
  envelope: {
    source: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ987'
    },
    destination: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ123'
    }
  },

  residentialSwitchOrderCancellationFailure: {
    faultCode: '501',
    faultText: 'Invalid or missing switch order reference',
    faultElement: 'switchOrderReference',
    faultElementValue: ''
  }
} as ResidentialSwitchOrderCancellationFailure;