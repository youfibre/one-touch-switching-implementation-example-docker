import { RepositoryBundle } from '../../data/repository-bundle.type';
import { OTSClient } from '@youfibre/otsc';
import { ResidentialSwitchMatchConfirmation } from '@youfibre/otsc';
import { ResidentialSwitchOrder } from '@youfibre/otsc';

export default function handleResidentialSwitchMatchConfirmation(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchMatchConfirmation
) {
  console.info(`\n\n✅ Residential Switch Match Confirmed\n\n`);
  
  // Find the related Switch Order
  const switchOrder = repositories.switchOrderRepository.findByCorrelationId(body.envelope.destination.correlationID);
  if (!switchOrder) {
    console.error(`🚨 Could not locate related Switch Order by Correlation ID: ${body.envelope.destination.correlationID}\n\n`);
    return;
  }

  switchOrder.currentStage = 'match';
  switchOrder.switchMatchRequest.switchMatchConfirmation = body;
  // At this stage the customer needs to select an option to switch to.
}