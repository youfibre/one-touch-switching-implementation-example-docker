import { RepositoryBundle } from '../../data/repository-bundle.type';
import { OTSClient } from '@youfibre/otsc';
import { ResidentialSwitchOrderConfirmation } from '@youfibre/otsc';

export default function handleResidentialSwitchOrderConfirmation(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderConfirmation
) {
  console.info(`\n\n✅ Residential Switch Order Confirmed\n\n`);

  // Find the related Switch Order
  const switchOrder = repositories.switchOrderRepository.findByCorrelationId(body.envelope.destination.correlationID);
  if (!switchOrder) {
    console.error(`🚨 Could not locate related Switch Order by Correlation ID: ${body.envelope.destination.correlationID}\n\n`);
    return;
  }

  switchOrder.currentStage = 'order';
}