import { RepositoryBundle } from '../../data/repository-bundle.type';
import { ResidentialSwitchOrderFailure } from '@youfibre/otsc';
import { OTSClient } from '@youfibre/otsc';

export default function handleResidentialSwitchOrderFailure(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderFailure
) {
  console.log('\n\n🚨 Received residentialSwitchOrderFailure: manual intervention required.');
  console.log(`🚨 Fault Code: ${body.residentialSwitchOrderFailure.faultCode}`);
  console.log(`🚨 Fault Reason: ${body.residentialSwitchOrderFailure.faultText}`);
  console.log(`🚨 Fault Element: ${body.residentialSwitchOrderFailure.faultElement || 'N/A'}`);
  console.log(`🚨 Fault Element Value: ${body.residentialSwitchOrderFailure.faultElementValue || 'N/A'}\n\n`);
}