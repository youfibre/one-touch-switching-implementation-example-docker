import { OTSClient } from '@youfibre/otsc';
import { RepositoryBundle } from '../../data/repository-bundle.type';
import { ResidentialSwitchOrder } from '@youfibre/otsc';
import { ResidentialSwitchOrderConfirmation } from '@youfibre/otsc';
import { ResidentialSwitchOrderFailure } from '@youfibre/otsc';
import * as nodemailer from 'nodemailer';

async function switchOrderNotFound(client: OTSClient, rcpId: string, body: ResidentialSwitchOrder) {
  return client.sendMessage({
    envelope: {
      source: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: body.envelope.destination.correlationID
      },
      destination: body.envelope.source
    },
  
    residentialSwitchOrderFailure: {
      faultCode: '201',
      faultText: 'Invalid or missing switch order reference',
      faultElement: 'switchOrderReference',
      faultElementValue: (body.residentialSwitchOrder.switchOrderReference || '').toString()
    }
  } as ResidentialSwitchOrderFailure);
}

async function accountNotRecognised(
  client: OTSClient,
  rcpId: string,
  body: ResidentialSwitchOrder,
  extra?: any
) {
  return client.sendMessage({
    envelope: {
      source: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: body.envelope.destination.correlationID
      },
      destination: body.envelope.source
    },
  
    residentialSwitchOrderFailure: {
      faultCode: '206',
      faultText: 'Account not recognised',
      ...extra
    }
  } as ResidentialSwitchOrderFailure);
}

export default async function handleResidentialSwitchOrder(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrder
) {
  console.info(`\n\n✅ Residential Switch Order Received\n\n`);
  
  const switchOrder = repositories.switchOrderRepository.findByOrderReference(body.residentialSwitchOrder.switchOrderReference);
  if (!switchOrder) {
    console.error(`🚨 Failed to find Switch Order by Order Reference: ${body.residentialSwitchOrder.switchOrderReference}`);
    return switchOrderNotFound(client, rcpId, body);
  }

  // Make sure that the services on the Switch Order haven't been used in another Switch Order which has become active.
  if (switchOrder.switchActions?.some(
    (sa) => sa.switchActionData.service && repositories.switchOrderRepository.hasActiveSwitchOrder(sa.switchActionData.service))
  ) {
    console.log(`Active Switch Order found for one or more services on Switch Order with Correlation ID ${switchOrder.correlationId} but notification format unknown to send to GP.`);
    return accountNotRecognised(client, rcpId, body, {
      faultElement: 'confusion',
      faultElementValue: 'Unsure of the format to inform GP that there is already an active Switch Order.'
    });
  }

  switchOrder.currentStage = 'order';
  switchOrder.switchDate = new Date(Date.parse(body.residentialSwitchOrder.plannedSwitchDate));
  
  const transport = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: parseInt(process.env.SMTP_PORT),
    auth: {
      user: process.env.SMTP_USERNAME,
      pass: process.env.SMTP_PASSWORD
    }
  });
  
  await transport.sendMail({
    subject: `It's official, you're leaving ${process.env.LOSING_FROM_NAME}!`,
    to: {
      address: switchOrder.customer.email,
      name: switchOrder.customer.name
    },
    from: {
      address: process.env.LOSING_FROM_EMAIL,
      name: process.env.LOSING_FROM_NAME
    },
    html: `
      <h1>We're sorry to see you go, ${switchOrder.customer.name}!</h1>
      <p>We've received confirmation that the switch we previously emailed you about is now commencing. We are sorry to see you go.</p>
      <p>Your services with us will remain as they are until your switch has been completed.</p>
      <p>If you need any help in the meantime, please contact us on 0800 123 1234.</p>
    `
  });

  return client.sendMessage({
    envelope: {
      source: body.envelope.destination,
      destination: body.envelope.source
    },
  
    residentialSwitchOrderConfirmation: {
      status: 'confirmed'
    }
  } as ResidentialSwitchOrderConfirmation);
}