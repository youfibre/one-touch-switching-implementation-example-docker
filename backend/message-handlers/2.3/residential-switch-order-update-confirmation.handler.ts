import { OTSClient } from '@youfibre/otsc';
import { RepositoryBundle } from '../../data/repository-bundle.type';
import { ResidentialSwitchOrderUpdateConfirmation } from '@youfibre/otsc';

export default function handleResidentialSwitchOrderUpdateConfirmation(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderUpdateConfirmation
) {
  console.info(`\n\n✅ Residential Switch Order Update Confirmed\n\n`);
}