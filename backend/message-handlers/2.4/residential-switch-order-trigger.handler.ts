import { OTSClient } from '@youfibre/otsc';
import { RepositoryBundle } from '../../data/repository-bundle.type';
import { ResidentialSwitchOrderTrigger } from '@youfibre/otsc';
import { ResidentialSwitchOrderTriggerConfirmation } from '@youfibre/otsc';
import { ResidentialSwitchOrderTriggerFailure } from '@youfibre/otsc';
import { VoiceService } from '../../data/customer-repository.class';
import * as nodemailer from 'nodemailer';

async function switchOrderNotFound(client: OTSClient, rcpId: string, body: ResidentialSwitchOrderTrigger) {
  return client.sendMessage({
    envelope: {
      source: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: body.envelope.destination.correlationID
      },
      destination: body.envelope.source
    },
  
    residentialSwitchOrderTriggerFailure: {
      faultCode: '401',
      faultText: 'Invalid or missing switch order reference',
      faultElement: 'switchOrderReference',
      faultElementValue: (body.residentialSwitchOrderTrigger.switchOrderReference || '').toString()
    }
  } as ResidentialSwitchOrderTriggerFailure);
}

export default async function handleResidentialSwitchOrderTrigger(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderTrigger
) {
  console.info(`\n\n✅ Residential Switch Order Trigger Received\n\n`);
  
  const switchOrder = repositories.switchOrderRepository.findByOrderReference(body.residentialSwitchOrderTrigger.switchOrderReference);
  if (!switchOrder) {
    console.error(`🚨 Failed to find Switch Order by Order Reference: ${body.residentialSwitchOrderTrigger.switchOrderReference}`);
    return switchOrderNotFound(client, rcpId, body);
  }

  switchOrder.currentStage = 'completed';

  // Once the switch order is completed, we send any final bills, terminate their services, etc.
  console.info(`📧\tSEND FINAL BILL TO CUSTOMER ${switchOrder.customer.name} <${switchOrder.customer.email}>`);
  const transport = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: parseInt(process.env.SMTP_PORT),
    auth: {
      user: process.env.SMTP_USERNAME,
      pass: process.env.SMTP_PASSWORD
    }
  });
  
  await transport.sendMail({
    subject: `Your Final ${process.env.LOSING_FROM_NAME} Bill`,
    to: {
      address: switchOrder.customer.email,
      name: switchOrder.customer.name
    },
    from: {
      address: process.env.LOSING_FROM_EMAIL,
      name: process.env.LOSING_FROM_NAME
    },
    html: `
      <h1>Here's your final bill, ${switchOrder.customer.name}.</h1>
      <p>Your new provider has informed us that your services are now fully installed and that we should proceed with the instant switching process.</p>
      <p>Your services with us will shortly be disconnected or ported, as previously mentioned.</p>
      <table>
        <tr style="border-bottom: 3px solid #000;">
          <td></td>
          <td><strong>£</strong></td>
        </tr>
        <tr>
          <td>Broadband Service</td>
          <td>£123.45</td>
        </tr>
        <tr>
          <td>Phone Service</td>
          <td>£434.32</td>
        </tr>
        <tr>
          <td></td>
          <td>---</td>
        </tr>
        <tr>
          <td>Amount Due</td>
          <td>£557.77</td>
        </tr>
      </table>
      <p><em>(this is an example, not representative of actual Switch Order details)</em></p>
      <p>If you need any help with this bill, please contact us on 0800 123 1234.</p>
    `
  });

  console.info(`❌\tDeprovision customer services: ${switchOrder.customer.name} <${switchOrder.customer.email}>`);
  switchOrder.switchActions?.forEach((sa) => {
    if (!sa.switchActionData.service) return;

    if (sa.switchActionData.switchAction === 'port') {
      console.info(`📝\tPerform any porting-related activities for ${(sa.switchActionData.service as VoiceService).phoneNumber}`);
    }

    const serviceIndex = switchOrder.customer.services.indexOf(sa.switchActionData.service);
    if (serviceIndex > -1) {
      switchOrder.customer.services.splice(serviceIndex, 1);
    }
  })
  
  if (switchOrder.customer.services.length === 0) {
    repositories.customerRepository.remove(switchOrder.customer.accountNumber);
    console.info(`🗑️\t\tRemove customer from billing system: ${switchOrder.customer.name} <${switchOrder.customer.email}>`);
  }

  console.log('\n');

  return client.sendMessage({
    envelope: {
      source: body.envelope.destination,
      destination: body.envelope.source
    },
  
    residentialSwitchOrderTriggerConfirmation: {
      status: 'triggered'
    }
  } as ResidentialSwitchOrderTriggerConfirmation);
}