import { RepositoryBundle } from '../../data/repository-bundle.type';
import { OTSClient } from '@youfibre/otsc';
import { ResidentialSwitchOrderCancellationConfirmation } from '@youfibre/otsc';

export default function handleResidentialSwitchOrderCancellationConfirmation(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderCancellationConfirmation
) {
  console.info(`\n\n✅ Residential Switch Order Cancellation Confirmed\n\n`);
}