import { Address } from '@youfibre/otsc';

export interface Service {
  serviceId: string;
  status: 'ordered' | 'installed';
}

export interface VoiceService extends Service {
  type: 'NBICS';
  phoneNumber: string;
}

export interface BroadbandService extends Service {
  type: 'IAS'
}

export interface Customer {
  accountNumber: string;
  name: string;
  email: string;
  uprn: number;
  address: Address;
  services: (BroadbandService | VoiceService)[];
}

export class CustomerRepository {
  private customers: Map<string, Customer>;

  constructor(initialCustomers: Customer[]) {
    this.customers = new Map<string, Customer>([]);
    initialCustomers.forEach((customer) => this.customers.set(customer.accountNumber, customer));
  }

  public findByUprn(uprn: number): Customer[] {
    // Definitely not efficient, avoid in production. :-)
    return Array.from(this.customers.values()).filter((c) => c.uprn === uprn);
  }

  public findByServiceId(serviceId: string): Customer {
    // Definitely not efficient, avoid in production. :-)
    return Array.from(this.customers.values()).find((c) => c.services.find((svc) => svc.serviceId === serviceId));
  }

  public get(id: string): Customer | undefined {
    return this.customers.get(id);
  }

  public add(customer: Customer): void {
    this.customers.set(customer.accountNumber, customer);
  }

  public remove(accountNumber: string): void {
    this.customers.delete(accountNumber);
  }

  public count(): number {
    return this.customers.size;
  }

  public asArray(): Customer[] {
    return Array.from(this.customers.values());
  }
}