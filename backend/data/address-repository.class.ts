import { Address } from '@youfibre/otsc';

// The purpose of this is to act like a source of address data (like PAF)
export class AddressRepository {
  private addresses: Map<number, Address>;

  constructor(initialAddresses: { [uprn: number]: Address }) {
    this.addresses = new Map<number, Address>();
    Object.keys(initialAddresses).forEach((uprn: string) => {
      try {
        this.addresses.set(parseInt(uprn), initialAddresses[uprn]);
      } catch {}
    });
  }

  public findByUprn(uprn: number): Address | undefined {
    return this.addresses.get(uprn);
  }

  public count(): number {
    return this.addresses.size;
  }

  public asArray(): { uprn: number, address: Address }[] {
    return Array.from(this.addresses.keys()).reduce((arr, uprn) => {
      const address = this.addresses.get(uprn);
      if (address) {
        arr.push({
          uprn,
          address
        });
      }
      return arr;
    }, [] as { uprn: number, address: Address }[]) || [];
  }
}