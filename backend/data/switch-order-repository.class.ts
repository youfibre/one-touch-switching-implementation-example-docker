import { ResidentialSwitchMatchConfirmation, ResidentialSwitchMatchFailure, ResidentialSwitchMatchRequest } from '@youfibre/otsc';
import { BroadbandService, Customer, VoiceService } from './customer-repository.class';

export type RequestedService = {
  serviceType: 'IAS';
  action: 'cease';
} | {
  serviceType: 'NBICS';
  serviceIdentifierType?: 'DN';
  serviceIdentifier?: string;
  action: 'cease' | 'port'
};

export type SwitchActionData = {
  switchAction: string,
  service?: BroadbandService | VoiceService
};

export type SwitchActionsArray = {
  requestedService: RequestedService,
  switchActionData: SwitchActionData
}[];

export interface SwitchOrder {
  currentStage: 'request' | 'match' | 'order' | 'completed' | 'cancelled';
  switchOrderReference: string;
  correlationId: string;
  customer: Customer;
  switchActions?: SwitchActionsArray; // Used only if losing a customer
  switchMatchRequest?: {
    request: ResidentialSwitchMatchRequest;
    relatedServiceIds: string[];
    switchMatchConfirmation?: ResidentialSwitchMatchConfirmation;
    switchMatchFailure?: ResidentialSwitchMatchFailure;
  }; // Used only if gaining a customer
  losingProviderRcpId: string;
  gainingProviderRcpId: string;
  switchDate?: Date;
}

export class SwitchOrderRepository {
  private switchOrders: SwitchOrder[];

  constructor() {
    this.switchOrders = [];
  }

  public add(switchOrder: SwitchOrder): void {
    this.switchOrders.push(switchOrder);
  }

  public count(): number {
    return this.switchOrders.length;
  }

  public findByCustomer(customer: Customer): SwitchOrder[] {
    return this.switchOrders.filter((so) => so.customer.accountNumber === customer.accountNumber);
  }

  public findByOrderReference(orderReference: string): SwitchOrder | undefined {
    return this.switchOrders.find((so) => so.switchOrderReference === orderReference);
  }

  public findByCorrelationId(correlationId: string): SwitchOrder | undefined {
    return this.switchOrders.find((so) => so.correlationId === correlationId);
  }

  public getActiveSwitchOrder(service: BroadbandService | VoiceService): SwitchOrder {
    for (const switchOrder of this.switchOrders) {
      if (switchOrder.currentStage !== 'order') continue;
      for (const switchAction of switchOrder.switchActions || []) {
        if (switchAction.switchActionData.service === service) {
          return switchOrder;
        }
      }

      for (const affectedServiceId of switchOrder.switchMatchRequest?.relatedServiceIds || []) {
        if (affectedServiceId === service.serviceId) {
          return switchOrder;
        }
      }
    }
    return null;
  }

  public hasActiveSwitchOrder(service: BroadbandService | VoiceService): boolean {
    return !!this.getActiveSwitchOrder(service);
  }

  public asArray(): SwitchOrder[] {
    return this.switchOrders;
  }
}